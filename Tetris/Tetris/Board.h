#pragma once

#include "Figura.h"
#include "BlockI.h"
#include "BlockJ.h"
#include "BlockL.h"
#include "BlockS.h"
#include "BlockT.h"
#include "BlockZ.h"
#include "blockO.h"
#include <string>

class Board
{
public:
	BlockI block_i;
	BlockJ block_j;
	BlockL block_l;
	BlockS block_s;
	BlockT block_t;
	BlockZ block_z;
	blockO block_o;
	int px = 7;
	int py = 0;
	int board[16][20];
	Board();
	void addFigure(Figura& fig);
	void addFigure(int figure);
	void addRandomFig();
};

