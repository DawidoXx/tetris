﻿// Tetris.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include <SFML/Graphics.hpp>
#include "Board.h"
#include "BlockI.h"

Board objBoard;

int main()
{
	//////////////////// INIT SHAPE FOR DRAWING ///////////////////////////////////////////////////////////////////////
	sf::RenderWindow window(sf::VideoMode(805,1005), "Tetris");
	sf::RectangleShape square(sf::Vector2f(45.0f, 45.0f));
	square.setFillColor(sf::Color::Red);
	square.setOutlineThickness(5.0f);
	square.setOutlineColor(sf::Color::Black);

	/////////////////////////// INIT BOARD ////////////////////////////////////////////////////////////////////////////
	objBoard.addRandomFig();
	


	sf::Event event;

	while(window.isOpen())
	{
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;

			case sf::Event::Resized:
				printf("New window hight: %i. New window width: %i.\n", event.size.height, event.size.width);
				break;

			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Left)
				{
					//move to the left
				}
				if (event.key.code == sf::Keyboard::Up)
				{
					//rotate
				}
				if (event.key.code == sf::Keyboard::Right)
				{
					//move to the right
				}
				break;
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				//move faster
			}
		}

		////////Draw board////////
		objBoard.addRandomFig();
		window.clear();

		for (int i=0; i<16; i++)
		{
			for(int j=0; j<20; j++)
			{
				if (objBoard.board[i][j] == 1) {
					square.setPosition(sf::Vector2f(50.0f * i+5.0f, 50.0f * j+5.0f));
					window.draw(square);
				}
			}
		}
		window.display();
	}
}
