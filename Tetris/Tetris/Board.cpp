#include "Board.h"

Board::Board()
{
	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			if (i == 0 || i == 15 || j == 19)
				board[i][j] = 1;
			else
				board[i][j] = 0;
		}
	}
}

void Board::addFigure(Figura& fig)
{
	for (int i=0;i<2;i++)
	{
		for(int j=0;j<4;j++)
		{
			board[i+px][j] = fig.tab[i][j];
		}
	}
}

void Board::addFigure(int figure)
{
	switch (figure)
	{
	case 0:
		addFigure(block_j);
		break;
	case 1:
		addFigure(block_i);
		break;
	case 2:
		addFigure(block_o);
		break;
	case 3:
		addFigure(block_s);
		break;
	case 4:
		addFigure(block_z);
		break;
	case 5:
		addFigure(block_l);
		break;
	case 6:
		addFigure(block_t);
		break;

	}
}

void Board::addRandomFig()
{
	addFigure(rand() % 7);
}


